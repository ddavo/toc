----------------------------------------------------------------------------------
-- Company:        Universidad Complutense de Madrid
-- Engineer:       
-- 
-- Create Date:    
-- Design Name:    Practica 1b 
-- Module Name:    divisor - rtl
-- Project Name:   Practica 1b 
-- Target Devices: Spartan-3 
-- Tool versions:  ISE 14.1
-- Description:    Creacion de un reloj de 1 Hz a partir de un reloj de 100 MHz
-- Dependencies: 
-- Revision: 
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use IEEE.MATH_REAL.ALL;

entity divisor is
  generic ( N : integer := 10**6 );
  port (
    rst        : in  std_logic;         -- asynch reset
    clk_100mhz : in  std_logic;         -- 100 MHz input clock
    clk_out    : out std_logic          -- 1 Hz output clock
    );
end divisor;

architecture rtl of divisor is
	constant N_BITS	: integer	:= integer(ceil(log2(real(N))));

  signal cntr_reg    : unsigned(N_BITS-1 downto 0) := to_unsigned(0, N_BITS);
  signal clk_out_reg : std_logic;
begin

  p_cntr : process(rst, clk_100mhz)
  begin
    if (rst = '0') then
      cntr_reg    <= (others => '0');
      clk_out_reg <= '0';
    elsif rising_edge(clk_100mhz) then
      if cntr_reg = to_unsigned(N, N_BITS) then
        cntr_reg    <= (others => '0');
        clk_out_reg <= not clk_out_reg;
      else
        cntr_reg    <= cntr_reg + 1;
        clk_out_reg <= clk_out_reg;
      end if;
    end if;
  end process p_cntr;

  output_clock : clk_out <= clk_out_reg;
end rtl;
