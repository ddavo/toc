--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   22:03:06 11/12/2018
-- Design Name:   
-- Module Name:   /home/davo/Documents/toc/Pr04/tb_multiplier.vhd
-- Project Name:  Pr04
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: multiplier
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL; 
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
ENTITY tb_multiplier IS
END tb_multiplier;
 
ARCHITECTURE behavior OF tb_multiplier IS 
	constant N : integer := 8;
	
	
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT multiplier
	 GENERIC ( N : integer );
    PORT(
         clk : IN  std_logic;
         rst : IN  std_logic;
         ini : IN  std_logic;
         x : IN  std_logic_vector(N-1 downto 0);
         y : IN  std_logic_vector(N-1 downto 0);
         z : OUT  std_logic_vector(2*N-1 downto 0);
         done : OUT  std_logic
         -- display : OUT  std_logic_vector(13 downto 0)
        );
    END COMPONENT;

   --Inputs
   signal clk : std_logic := '0';
   signal rst : std_logic := '0';
   signal ini : std_logic := '0';
   signal x : std_logic_vector(N-1 downto 0) := (others => '0');
   signal y : std_logic_vector(N-1 downto 0) := (others => '0');

 	--Outputs
   signal z : std_logic_vector(2*N-1 downto 0);
   signal done : std_logic;
   -- signal display : std_logic_vector(13 downto 0);

   -- Clock period definitions
   constant clk_period : time := 10 ns;
	
	-- Expected z
	signal zexp : unsigned(2*N-1 downto 0);
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: multiplier
	GENERIC MAP (
			 N => N
	)
	PORT MAP (
          clk => clk,
          rst => rst,
          ini => ini,
          x => x,
          y => y,
          z => z,
          done => done
          -- display => display
        );

   -- Clock process definitions
   clk_process :process
   begin
		clk <= '0';
		wait for clk_period/2;
		clk <= '1';
		wait for clk_period/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
   begin
		wait for 2*clk_period;
		rst <= '1';
		ini <= '0';
      for i in 16 to 63 loop
			for j in 16 to 63 loop
				x <= std_logic_vector(to_unsigned(i, N));
				y <= std_logic_vector(to_unsigned(j, N));
				zexp <= to_unsigned(i*j, 2*N);
				ini <= '1';
				wait for clk_period;
				ini <= '0';
				wait until done = '1';
				-- wait for clk_period*10;
				-- wait for done = '1'
				assert unsigned(z) = zexp
					report "Error: Multiplicacin A incorrecta"
					severity error;
			end loop;
		end loop;

      wait;
   end process;

END;
