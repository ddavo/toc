----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    22:55:54 11/11/2018 
-- Design Name: 
-- Module Name:    pipo_8b - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity pipo_4b is port (
	x				: in std_logic_vector(3 downto 0);
	z				: out std_logic_vector(3 downto 0);
	clk, rst		: in std_logic;
	load			: in std_logic
);
end pipo_4b;

architecture Behavioral of pipo_4b is
	signal mem_int	: std_logic_vector(3 downto 0);
begin
	z <= mem_int;

	p_pipo : process(clk, rst) is
	begin
		if rst = '0' then
			mem_int <= (others => '0');
		elsif rising_edge(clk) and load = '1' then
			mem_int <= x;
		end if;	
	end process p_pipo;

end Behavioral;

