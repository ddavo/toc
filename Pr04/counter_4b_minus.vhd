----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    23:22:09 11/11/2018 
-- Design Name: 
-- Module Name:    counter_4b_minus - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity counter_4b_down is 
generic ( N : integer);
port(
	x		: in std_logic_vector(N-1 downto 0);
	load	: in std_logic;
	clk	: in std_logic;
	cd		: in std_logic;
	z		: out std_logic_vector(N-1 downto 0)
);
end counter_4b_down;
architecture Behavioral of counter_4b_down is
	signal mem_int : unsigned(N-1 downto 0);
begin
	p_cnt : process(clk, load, x) is
	begin
		if load = '1' then
			mem_int <= unsigned(x);
		elsif rising_edge(clk) and cd = '1' then
			mem_int <= mem_int - 1;
		end if;
	end process p_cnt;
	
	z <= std_logic_vector(mem_int);
end Behavioral;

