/**********************************************************************/
/*   ____  ____                                                       */
/*  /   /\/   /                                                       */
/* /___/  \  /                                                        */
/* \   \   \/                                                       */
/*  \   \        Copyright (c) 2003-2009 Xilinx, Inc.                */
/*  /   /          All Right Reserved.                                 */
/* /---/   /\                                                         */
/* \   \  /  \                                                      */
/*  \___\/\___\                                                    */
/***********************************************************************/

/* This file is designed for use with ISim build 0xfbc00daa */

#define XSI_HIDE_SYMBOL_SPEC true
#include "xsi.h"
#include <memory.h>
#ifdef __GNUC__
#include <stdlib.h>
#else
#include <malloc.h>
#define alloca _alloca
#endif
static const char *ng0 = "/home/davo/Documents/toc/Pr04/tb_multiplier.vhd";
extern char *IEEE_P_1242562249;

char *ieee_p_1242562249_sub_10420449594411817395_1035706684(char *, char *, int , int );
unsigned char ieee_p_1242562249_sub_1434214030532789707_1035706684(char *, char *, char *, char *, char *);


static void work_a_1432251444_2372691052_p_0(char *t0)
{
    char *t1;
    char *t2;
    char *t3;
    char *t4;
    char *t5;
    char *t6;
    int64 t7;
    int64 t8;

LAB0:    t1 = (t0 + 3544U);
    t2 = *((char **)t1);
    if (t2 == 0)
        goto LAB2;

LAB3:    goto *t2;

LAB2:    xsi_set_current_line(96, ng0);
    t2 = (t0 + 4192);
    t3 = (t2 + 56U);
    t4 = *((char **)t3);
    t5 = (t4 + 56U);
    t6 = *((char **)t5);
    *((unsigned char *)t6) = (unsigned char)2;
    xsi_driver_first_trans_fast(t2);
    xsi_set_current_line(97, ng0);
    t2 = (t0 + 2568U);
    t3 = *((char **)t2);
    t7 = *((int64 *)t3);
    t8 = (t7 / 2);
    t2 = (t0 + 3352);
    xsi_process_wait(t2, t8);

LAB6:    *((char **)t1) = &&LAB7;

LAB1:    return;
LAB4:    xsi_set_current_line(98, ng0);
    t2 = (t0 + 4192);
    t3 = (t2 + 56U);
    t4 = *((char **)t3);
    t5 = (t4 + 56U);
    t6 = *((char **)t5);
    *((unsigned char *)t6) = (unsigned char)3;
    xsi_driver_first_trans_fast(t2);
    xsi_set_current_line(99, ng0);
    t2 = (t0 + 2568U);
    t3 = *((char **)t2);
    t7 = *((int64 *)t3);
    t8 = (t7 / 2);
    t2 = (t0 + 3352);
    xsi_process_wait(t2, t8);

LAB10:    *((char **)t1) = &&LAB11;
    goto LAB1;

LAB5:    goto LAB4;

LAB7:    goto LAB5;

LAB8:    goto LAB2;

LAB9:    goto LAB8;

LAB11:    goto LAB9;

}

static void work_a_1432251444_2372691052_p_1(char *t0)
{
    char t13[16];
    char *t1;
    char *t2;
    char *t3;
    int64 t4;
    int64 t5;
    char *t6;
    char *t7;
    char *t8;
    int t9;
    int t10;
    int t11;
    int t12;
    char *t14;
    char *t15;
    int t16;
    char *t17;
    char *t18;
    char *t19;
    char *t20;
    char *t21;
    int t22;
    int t23;
    int t24;
    int t25;
    unsigned int t26;
    unsigned char t27;
    unsigned char t28;

LAB0:    t1 = (t0 + 3792U);
    t2 = *((char **)t1);
    if (t2 == 0)
        goto LAB2;

LAB3:    goto *t2;

LAB2:    xsi_set_current_line(106, ng0);
    t2 = (t0 + 2568U);
    t3 = *((char **)t2);
    t4 = *((int64 *)t3);
    t5 = (2 * t4);
    t2 = (t0 + 3600);
    xsi_process_wait(t2, t5);

LAB6:    *((char **)t1) = &&LAB7;

LAB1:    return;
LAB4:    xsi_set_current_line(107, ng0);
    t2 = (t0 + 4256);
    t3 = (t2 + 56U);
    t6 = *((char **)t3);
    t7 = (t6 + 56U);
    t8 = *((char **)t7);
    *((unsigned char *)t8) = (unsigned char)3;
    xsi_driver_first_trans_fast(t2);
    xsi_set_current_line(108, ng0);
    t2 = (t0 + 4320);
    t3 = (t2 + 56U);
    t6 = *((char **)t3);
    t7 = (t6 + 56U);
    t8 = *((char **)t7);
    *((unsigned char *)t8) = (unsigned char)2;
    xsi_driver_first_trans_fast(t2);
    xsi_set_current_line(109, ng0);
    t2 = (t0 + 6808);
    *((int *)t2) = 16;
    t3 = (t0 + 6812);
    *((int *)t3) = 63;
    t9 = 16;
    t10 = 63;

LAB8:    if (t9 <= t10)
        goto LAB9;

LAB11:    xsi_set_current_line(126, ng0);

LAB32:    *((char **)t1) = &&LAB33;
    goto LAB1;

LAB5:    goto LAB4;

LAB7:    goto LAB5;

LAB9:    xsi_set_current_line(110, ng0);
    t6 = (t0 + 6816);
    *((int *)t6) = 16;
    t7 = (t0 + 6820);
    *((int *)t7) = 63;
    t11 = 16;
    t12 = 63;

LAB12:    if (t11 <= t12)
        goto LAB13;

LAB15:
LAB10:    t2 = (t0 + 6808);
    t9 = *((int *)t2);
    t3 = (t0 + 6812);
    t10 = *((int *)t3);
    if (t9 == t10)
        goto LAB11;

LAB29:    t11 = (t9 + 1);
    t9 = t11;
    t6 = (t0 + 6808);
    *((int *)t6) = t9;
    goto LAB8;

LAB13:    xsi_set_current_line(111, ng0);
    t8 = (t0 + 6808);
    t14 = (t0 + 2448U);
    t15 = *((char **)t14);
    t16 = *((int *)t15);
    t14 = ieee_p_1242562249_sub_10420449594411817395_1035706684(IEEE_P_1242562249, t13, *((int *)t8), t16);
    t17 = (t0 + 4384);
    t18 = (t17 + 56U);
    t19 = *((char **)t18);
    t20 = (t19 + 56U);
    t21 = *((char **)t20);
    memcpy(t21, t14, 8U);
    xsi_driver_first_trans_fast(t17);
    xsi_set_current_line(112, ng0);
    t2 = (t0 + 6816);
    t3 = (t0 + 2448U);
    t6 = *((char **)t3);
    t16 = *((int *)t6);
    t3 = ieee_p_1242562249_sub_10420449594411817395_1035706684(IEEE_P_1242562249, t13, *((int *)t2), t16);
    t7 = (t0 + 4448);
    t8 = (t7 + 56U);
    t14 = *((char **)t8);
    t15 = (t14 + 56U);
    t17 = *((char **)t15);
    memcpy(t17, t3, 8U);
    xsi_driver_first_trans_fast(t7);
    xsi_set_current_line(113, ng0);
    t2 = (t0 + 6808);
    t3 = (t0 + 6816);
    t16 = *((int *)t2);
    t22 = *((int *)t3);
    t23 = (t16 * t22);
    t6 = (t0 + 2448U);
    t7 = *((char **)t6);
    t24 = *((int *)t7);
    t25 = (2 * t24);
    t6 = ieee_p_1242562249_sub_10420449594411817395_1035706684(IEEE_P_1242562249, t13, t23, t25);
    t8 = (t13 + 12U);
    t26 = *((unsigned int *)t8);
    t26 = (t26 * 1U);
    t27 = (16U != t26);
    if (t27 == 1)
        goto LAB16;

LAB17:    t14 = (t0 + 4512);
    t15 = (t14 + 56U);
    t17 = *((char **)t15);
    t18 = (t17 + 56U);
    t19 = *((char **)t18);
    memcpy(t19, t6, 16U);
    xsi_driver_first_trans_fast(t14);
    xsi_set_current_line(114, ng0);
    t2 = (t0 + 4320);
    t3 = (t2 + 56U);
    t6 = *((char **)t3);
    t7 = (t6 + 56U);
    t8 = *((char **)t7);
    *((unsigned char *)t8) = (unsigned char)3;
    xsi_driver_first_trans_fast(t2);
    xsi_set_current_line(115, ng0);
    t2 = (t0 + 2568U);
    t3 = *((char **)t2);
    t4 = *((int64 *)t3);
    t2 = (t0 + 3600);
    xsi_process_wait(t2, t4);

LAB20:    *((char **)t1) = &&LAB21;
    goto LAB1;

LAB14:    t2 = (t0 + 6816);
    t11 = *((int *)t2);
    t3 = (t0 + 6820);
    t12 = *((int *)t3);
    if (t11 == t12)
        goto LAB15;

LAB28:    t16 = (t11 + 1);
    t11 = t16;
    t6 = (t0 + 6816);
    *((int *)t6) = t11;
    goto LAB12;

LAB16:    xsi_size_not_matching(16U, t26, 0);
    goto LAB17;

LAB18:    xsi_set_current_line(116, ng0);
    t2 = (t0 + 4320);
    t3 = (t2 + 56U);
    t6 = *((char **)t3);
    t7 = (t6 + 56U);
    t8 = *((char **)t7);
    *((unsigned char *)t8) = (unsigned char)2;
    xsi_driver_first_trans_fast(t2);
    xsi_set_current_line(117, ng0);

LAB24:    t2 = (t0 + 4112);
    *((int *)t2) = 1;
    *((char **)t1) = &&LAB25;
    goto LAB1;

LAB19:    goto LAB18;

LAB21:    goto LAB19;

LAB22:    t3 = (t0 + 4112);
    *((int *)t3) = 0;
    xsi_set_current_line(120, ng0);
    t2 = (t0 + 1832U);
    t3 = *((char **)t2);
    t2 = (t0 + 6704U);
    t6 = (t0 + 2152U);
    t7 = *((char **)t6);
    t6 = (t0 + 6720U);
    t27 = ieee_p_1242562249_sub_1434214030532789707_1035706684(IEEE_P_1242562249, t3, t2, t7, t6);
    if (t27 == 0)
        goto LAB26;

LAB27:    goto LAB14;

LAB23:    t3 = (t0 + 1992U);
    t6 = *((char **)t3);
    t27 = *((unsigned char *)t6);
    t28 = (t27 == (unsigned char)3);
    if (t28 == 1)
        goto LAB22;
    else
        goto LAB24;

LAB25:    goto LAB23;

LAB26:    t8 = (t0 + 6824);
    xsi_report(t8, 33U, (unsigned char)2);
    goto LAB27;

LAB30:    goto LAB2;

LAB31:    goto LAB30;

LAB33:    goto LAB31;

}


extern void work_a_1432251444_2372691052_init()
{
	static char *pe[] = {(void *)work_a_1432251444_2372691052_p_0,(void *)work_a_1432251444_2372691052_p_1};
	xsi_register_didat("work_a_1432251444_2372691052", "isim/tb_multiplier_isim_beh.exe.sim/work/a_1432251444_2372691052.didat");
	xsi_register_executes(pe);
}
