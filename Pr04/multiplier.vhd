----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    22:46:05 11/10/2018 
-- Design Name: 
-- Module Name:    multiplier - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;
entity multiplier is
generic ( N: integer := 8 );
port (
	clk		: in std_logic;
	rst		: in std_logic;
	ini		: in std_logic;
	x,y		: in std_logic_vector(N-1 downto 0);
	z			: out std_logic_vector(N*2-1 downto 0);
	done		: out std_logic
--	display	: out std_logic_vector(13 downto 0)
);
end multiplier;
architecture struct of multiplier is
--	component conv_7seg is port (
--		x			: in std_logic_vector(3 downto 0);
--		display	: out std_logic_vector(6 downto 0)
--	);
--	end component conv_7seg;
	
	component cd is 
	generic ( N : integer );
	port (
		clk		: in std_logic;
		rst		: in std_logic;
		ctrl		: in std_logic_vector(4 downto 0);
		status	: out std_logic;
		a_in		: in std_logic_vector(N-1 downto 0);	-- DATA INPUT
		b_in		: in std_logic_vector(N-1 downto 0);	-- DATA INPUT
		z			: out std_logic_vector(2*N-1 downto 0)	-- DATA OUTPUT
	);
	end component cd;
	
	component uc is 
	port (
		clk		: in std_logic;
		rst		: in std_logic;
		ini		: in std_logic;	-- CONTROL INPUT
		done		: out std_logic;	-- CONTROL OUTPUT
		ctrl		: out std_logic_vector(4 downto 0);
		status	: in std_logic
	);
	end component uc;
	
	COMPONENT debouncer is
	PORT(
		rst : IN std_logic;
		clk : IN std_logic;
		x : IN std_logic;          
		xdeb : OUT std_logic;
		xdebfallingedge : OUT std_logic;
		xdebrisingedge : OUT std_logic
		);
	END COMPONENT;
	
	signal xdeb    : std_logic;
	signal z_in		: std_logic_vector(2*N-1 downto 0);
	signal status	: std_logic;
	signal ctrl		: std_logic_vector(4 downto 0);
begin
	z <= z_in;
	
	i_uc : uc port map (
		clk		=> clk,
		rst		=> rst,
		ini	 	=> xdeb,
		done		=> done,
		ctrl		=> ctrl,
		status	=> status
	);
	
	i_cd : cd 
	generic map (
		N => N )
	port map (
		clk		=> clk,
		rst		=> rst,
		ctrl		=> ctrl,
		status	=> status,
		a_in		=> x,
		b_in		=> y,
		z			=> z_in
	);

--	i_conv_l : conv_7seg port map (
--		x 			=> z_in (7 downto 4),
--		display	=> display (13 downto 7)
--	);
--	
--	i_conv_r : conv_7seg port map (
--		x			=> z_in (3 downto 0),
--		display	=> display (6 downto 0)
--	);
	
	-- COMENTAR EN SIMULACION
--	i_debouncer : debouncer port map (
--		clk		=> clk,
--		rst		=> rst,
--		x 			=> ini,
--		xdebfallingedge => xdeb
--	);
	
	-- DESCOMENTAR EN SIMULACION
	xdeb <= ini;

end struct;

