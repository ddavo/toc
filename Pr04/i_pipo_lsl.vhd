----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    22:33:40 11/11/2018 
-- Design Name: 
-- Module Name:    pipo_8b_lsr - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity pipo_8b_lsl is 
generic ( N: integer );
port (
	x				: in std_logic_vector(N-1 downto 0);
	z				: out std_logic_vector(N-1 downto 0);
	clk, rst		: in std_logic;
	load, shift	: in std_logic
);
end pipo_8b_lsl;

architecture Behavioral of pipo_8b_lsl is
signal mem_int	: std_logic_vector(N-1 downto 0);
begin
	z <= mem_int;

	p_pipo_lsl	: process (clk, rst) is
	begin
		if rst = '0' then
			mem_int <= (others => '0');
		elsif rising_edge(clk) then
			if load = '1' then
				mem_int <= x;
			elsif shift = '1' then
				mem_int <= mem_int(N-2 downto 0) & '0';
			end if;
		end if;
	end process p_pipo_lsl;
end Behavioral;

