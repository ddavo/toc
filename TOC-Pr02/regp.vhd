----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    13:31:24 10/15/2018 
-- Design Name: 
-- Module Name:    regp - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity regp is
end regp;

architecture Behavioral of regp is

begin

p_reg : process (clk, rst) is
begin
if rising_edge(rst) then
dout <= (others => '0');
elsif rising_edge(clk) then
dout <= din;
end if;
end process p_reg;


end Behavioral;

